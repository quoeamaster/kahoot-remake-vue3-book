
## Kahoot remake - vue3 book example

As an example for the book [build real world Vue3 apps with Vite, TailwindCSS and Elasticsearch](https://www.amazon.com/); we are going to build a [Kahoot](https://kahoot.com/what-is-kahoot) style game with [Vue3.js](https://vuejs.org/) and [Vite](https://vitejs.dev/) and [BootstrapCSS](https://getbootstrap.com/docs/3.4/css/). 

Note that Kahoot is composed of the client-side and admin-side web apps. In this code base, only the client-side web app would be created.

#### features

   - able to load a set of questions and choices through config file(s)
   - render and display the set of questions in a random fashion
   - tally on answers

#### page flow

   - welcome / title page (start game or hall of fame) -> hall-of-fame = the results tally for historical games (e.g. 2 times 10 straight, 1 time 7 straight etc)
   - game page (displaying sets of questions for answering + tallying)
   - conclusion page (showing results of the game play)
