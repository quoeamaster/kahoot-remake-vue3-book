// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// Kahoot remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// setup vuex
import { createStore } from 'vuex'

// setup store
const store = createStore({
   state() {
      return {
         // results per game-play
         // a number with how many correct answers obtained per game-play
         results: { '0': 0, '1': 0, '2': 0, '3': 0, },

         // the to be rendered question set and answers...
         // { "q": [ { "q": "what is elephant in german?", "a": [ "euro", "alpha", "calve", "dolceVita" ], "c": "calve" }, {}... ] }
         questionSet: {},

         // the current result of the game play
         currentCorrectHits: 0,
      };
   },
   mutations: {
      // addEvent - add the "# of wins" to the events array
      updateResults(state, wins) {
         state.results[`${wins}`] = state.results[`${wins}`]+1;
      },
      setQuestionSet(state, payload) {
         state.questionSet = payload;
      },
      setCurrentCorrectHits(state, wins) {
         state.currentCorrectHits = wins;
      }
   },
})

export default store;